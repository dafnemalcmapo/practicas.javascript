// declaracion de arreglos
var arreglo = [19,20,3,150,6,3,3,9];

// Mostrar los elementos de arreglos

function mostrarArray(){
    for(con=0 ; con < arreglo.length ; ++con ){
        
        console.log(con + ":" + arreglo[con]);

    }
}

//funcion que regresa el promedio de los elementos del arreglo

function promedio(){
    let pro = 0;
    for(con=0 ; con < arreglo.length ; ++con ){
        pro += arreglo[con];
    }
    pro = pro/arreglo.length;
    return pro;
}

// Realizar una funcion que regrese un valor entero que represente la cantidad de numeros pares en el arreglo
 function par(){
    let par=0;
    for(con=0 ; con < arreglo.length ; ++con ){
        if(arreglo[con]%2==0){
            par++
        }
    }
 return par;
}

//Realizar una funcion que te regrese el calor mayor, contenido en el array
function mayor(){
    let mayor=0;
    for(con=0 ; con < arreglo.length ; ++con ){
        if(arreglo[con]>=mayor){
            mayor=arreglo[con];
            
        }
    }
    return mayor;
}

//diseñe una funcion que recibe un valor numerico que indica la cantidad de elementos del arreglo
//el arreglo debera de llenarse con valores aleatorios en el rango del 0 al 1000
//posteriormente mostrara el arreglo 

function valor(cant){
    const arreglo = [];
    for(let con = 0; con < cant; ++con){
        const numeroAleatorio = Math.floor(Math.random() * 1000);
        arreglo.push(numeroAleatorio);
    }
    console.log(arreglo);
}




mostrarArray();
//1
const prom = promedio();
console.log("El promedio es " + prom);

//2
const pares = par();
console.log("El numero de pares es: " + pares);

//3
const Mayor = mayor();
console.log("El numero mayor es: " + Mayor);

//4
const cant = 10;
valor(cant); 